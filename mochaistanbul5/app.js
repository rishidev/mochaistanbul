var express = require("express");

var app = express()

var getUsers = require("./users.js")
var users = getUsers()

app.get("/users/", function(request, response){
	if(users.length == 0){
		response.json([{name: "Ryan Dahl", age:50}])
	} else{
		response.json(users)
	}
})

// app.listen(8080)

module.exports = app

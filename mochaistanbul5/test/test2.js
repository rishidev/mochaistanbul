var request = require("request");
var assert = require("chai").assert;
var supertest = require("supertest");

var app = require("../app.js")

describe("API Test", function(){
	it("Getting users - 200", function(done){
		request('http://127.0.0.1:8080/users/', function (error, response, body) {
		  console.log('error:', error); // Print the error if one occurred
		  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
		  console.log('body:', body); // Print the HTML for the Google homepage.
		  assert.equal(response.statusCode, 200)
		});	
		done()
	})

	it("Getting users - 200", function(done){
		supertest(app).
		get("/users/").expect(200).
		expect("Content-type", /json/).
		end(function(err, response){
			console.log()
		})
		done()
	})
})
	
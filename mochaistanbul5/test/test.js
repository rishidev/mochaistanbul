var test = require("../index.js")
var assert = require("chai").assert

describe("Arithmetic opeartions:-", function(){
	it("Addition", function(done){
		var result = test.addition(12, 5)
		assert.equal(result, 17)
		done()
	})

	it("Subtraction", function(done){
		var result = test.subtraction(12, 5)
		assert.equal(result, 7)

		done()
	})

	it("Multiplication", function(done){
		var result = test.multiplication(12, 5)
		assert.equal(result, 60)
		done()
	})

	it("Division", function(done){
		var result = test.division(12, 3)
		assert.equal(result, 4)
		done()
	})
})
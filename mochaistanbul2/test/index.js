var details = require("../index.js")
// require("chai").should()
var assert = require("chai").assert

describe("Code Coverage", () => {
	assert.typeOf(details("Rishikesh", 25, "Bangalore"), "string")

	var sum = 0
	for(var item of [1,9,3]){
		// console.log("D")
		sum += item
	}

	it("Sum of [1,9,3] is 13", () => {
		assert.equal(sum, 13)
	})
	 
	it("Sum of [1,9,3]-3 is 10", () => {
		if(sum == 0) {
			// console.log("A")
			assert.equal(sum, 14)
		} else {
			// console.log("B")
			assert.equal(sum - 3, 10)
		}
	})
})

var multiplication = function (a, b){
	return a * b
}

var division = function (a, b){
	return a / b
}

module.exports = {
	multiplication: multiplication,
	division: division
}
var test2 = require("./index2.js")

var addition = function (a, b){
	if(a == 0){
		return b
	}
	return a + b
}

var subtraction = function (a, b){
	if(b == 0){
		return a
	} else if (a == 0) {
		return -b
	}

	return a - b
}

module.exports = {
	addition: addition,
	subtraction: subtraction,
	multiplication: test2.multiplication,
	division: test2.division
}

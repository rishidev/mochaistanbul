var test = require("../index.js")
var assert = require("chai").assert

describe("Arithmetic opeartions:-", function(){
	it("Addition", function(){
		var result = test.addition(12, 5)
		assert.equal(result, 17)
	})

	it("Subtraction", function(){
		var result = test.subtraction(12, 5)
		assert.equal(result, 7)
	})

	it("Multiplication", function(){
		var result = test.multiplication(12, 5)
		assert.equal(result, 60)
	})

	it("Division", function(){
		var result = test.division(12, 3)
		assert.equal(result, 4)
	})
})